import os
import re
from glob import iglob

from setuptools import setup, find_packages

ROOT_MODULE_NAME = 'cacheable_iter'
SOURCES_DIR = 'src'

requirements = []
with open('requirements.txt') as f:
    # noinspection PyRedeclaration
    requirements = f.read().splitlines()

with open(f'{SOURCES_DIR}/{ROOT_MODULE_NAME}/__init__.py') as f:
    # noinspection PyRedeclaration
    content = f.read()
    name    = re.search(r'^__title__\s*=\s*[\'"]([^\'"]*)[\'"]',   content, re.MULTILINE).group(1)
    version = re.search(r'^__version__\s*=\s*[\'"]([^\'"]*)[\'"]', content, re.MULTILINE).group(1)

if (not name):
    raise RuntimeError("Package name is not set")

if (not version):
    raise RuntimeError("Package version is not set")

if (version.endswith(('a', 'b', 'rc'))):
    # append version identifier based on commit count
    try:
        import subprocess
        p = subprocess.Popen(['git', 'rev-list', '--count', 'HEAD'], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = p.communicate()
        if out:
            version += out.decode('utf-8').strip()
        p = subprocess.Popen(['git', 'rev-parse', '--short', 'HEAD'], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = p.communicate()
        if out:
            version += '+g' + out.decode('utf-8').strip()
    except Exception:
        pass

readme = ''
with open('README.md') as f:
    # noinspection PyRedeclaration
    readme = f.read()

setup_requires = [ 'wheel' ]
for r in [ 'requirements/setup-requirements.txt', 'setup-requirements.txt' ]:
    if (os.path.isfile(r)):
        with open(r) as f:
            setup_requires = [ l.strip() for l in f ]

tests_require = [ ]
for r in [ 'requirements/test-requirements.txt', 'test-requirements.txt' ]:
    if (os.path.isfile(r)):
        with open(r) as f:
            tests_require = [ l.strip() for l in f]

extras_require = { }
for r in iglob('requirements/requirements-*.txt'):
    with open(r) as f:
        reqs = [ l.strip() for l in f ]
        feature_name = re.match(r'requirements-(.*)\.txt', os.path.basename(r)).group(1).title()
        extras_require[feature_name] = reqs
extras_require.setdefault('all', sum(extras_require.values(), list()))

setup \
(
    name = name,
    url = f'https://gitlab.com/Hares-Lab/{name}',
    version = version,
    packages = find_packages(SOURCES_DIR),
    package_dir = { '': SOURCES_DIR },
    license = "BSD 2-Clause License",
    description = "Python decorators for caching functions returning iterators",
    long_description = readme,
    long_description_content_type = 'text/markdown',
    include_package_data = True,
    setup_requires = setup_requires,
    install_requires = requirements,
    tests_require = tests_require,
    extras_require = extras_require,
    python_requires = '>=3.6.0',
    classifiers =
    [
        'Development Status :: 2 - Pre-Alpha',
        'License :: OSI Approved :: BSD License',
        'Intended Audience :: Developers',
        'Natural Language :: English',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
        'Topic :: Software Development :: Libraries',
        'Topic :: Software Development :: Libraries :: Python Modules',
        'Topic :: Utilities',
    ]
)
