from typing import *
try:
    from unittest import IsolatedAsyncioTestCase
except ImportError:
    from aiounittest import AsyncTestCase as IsolatedAsyncioTestCase

from async_lru import alru_cache

from cacheable_iter.core import alru_iter_cache
from tests.abstract_testcase import AbstractCachedIteratorTestCase

IntAwaitableIteratorFunction = Callable[[int], Awaitable[Iterator[int]]]
class CachedAwaitableIteratorTest(AbstractCachedIteratorTestCase, IsolatedAsyncioTestCase):
    @property
    def _test_subject(self) -> IntAwaitableIteratorFunction:
        return type(self).awaitable_iterator_function
    
    # region Extended Assertions
    async def assertIteration(self, func: IntAwaitableIteratorFunction, *args, steps: int = 5, **kwargs):
        return self._assertIteration(await func(steps), *args, **kwargs, steps=steps)
    # endregion
    # region Tests
    async def test_no_decorations(self):
        func = self.get_test_function()
        
        await self.assertIteration(func, "First")
        await self.assertIteration(func, "Second")
    
    async def test_alru_simple_cache(self):
        func = self.get_test_function(alru_cache)
        
        await self.assertIteration(func, "First", assert_counting=True)
        await self.assertIteration(func, "Second", assert_counting=False)
    
    async def test_alru_iter_cache(self):
        func = self.get_test_function(alru_iter_cache)
        
        await self.assertIteration(func, "First", expect_steps_increment=True, expect_calls_increment=True)
        await self.assertIteration(func, "Second", expect_steps_increment=False, expect_calls_increment=False)
    
    async def test_alru_iter_cache_with_maxsize(self):
        func = self.get_test_function(alru_iter_cache(maxsize=2))
        
        await self.assertIteration(func, "First",  expect_steps_increment=True,  expect_calls_increment=True,  steps=3)
        await self.assertIteration(func, "Second", expect_steps_increment=True,  expect_calls_increment=True,  steps=4)
        await self.assertIteration(func, "Third",  expect_steps_increment=False, expect_calls_increment=False, steps=3)
        await self.assertIteration(func, "Fourth", expect_steps_increment=True,  expect_calls_increment=True,  steps=5)
        await self.assertIteration(func, "Fifth",  expect_steps_increment=False, expect_calls_increment=False, steps=3)
        await self.assertIteration(func, "Sixth",  expect_steps_increment=True,  expect_calls_increment=True,  steps=4)
    
    async def test_alru_iter_cache_race(self):
        func = self.get_test_function(alru_iter_cache)
        
        # Start
        iter1 = await func(5)
        iter2 = await func(5)
        
        self.assertRaceIteration(iter1, 0, 0, "Initial", expect_total_calls=0, expect_total_steps=0)
        self.assertRaceIteration(iter1, 0, 2, "First",   expect_total_calls=1, expect_total_steps=2, iter_name='iter1')
        self.assertRaceIteration(iter2, 0, 2, "Second",  expect_total_calls=1, expect_total_steps=2, iter_name='iter2')
        self.assertRaceIteration(iter2, 2, 4, "Third",   expect_total_calls=1, expect_total_steps=4, iter_name='iter2')
        self.assertRaceIteration(iter1, 2, 5, "Fourth",  expect_total_calls=1, expect_total_steps=5, iter_name='iter1')
        self.assertRaceIteration(iter2, 4, 5, "Fifth",   expect_total_calls=1, expect_total_steps=5, iter_name='iter2')
        self.assertStopIteration(iter2, "Final", iter_name='iter2')
    # endregion


__all__ = \
[
    'CachedAwaitableIteratorTest',
]


if (__name__ == '__main__'):
    from unittest import main
    main()
