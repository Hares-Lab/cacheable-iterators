import asyncio
from abc import ABC
from functools import partial
from typing import *
from unittest import TestCase

I = TypeVar('I')
F = Callable[[int], I]
class AbstractCachedIteratorTestCase(TestCase, Generic[I], ABC):
    iterator_calls: int = 0
    iterator_steps: int = 0
    sleep_delay: float = 0.0
    _test_subject: F
    
    def setUp(self):
        self.iterator_calls = 0
        self.iterator_steps = 0
    
    # region Test Subjects
    def iterator_function(self, n: int) -> Iterator[int]:
        self.iterator_calls += 1
        for i in range(n):
            self.iterator_steps += 1
            yield i
    
    async def awaitable_iterator_function(self, n: int) -> Iterator[int]:
        gen = self.iterator_function(n)
        await asyncio.sleep(self.sleep_delay)
        return gen
    
    async def async_iterator_function(self, n: int) -> AsyncIterator[int]:
        for _ in await self.awaitable_iterator_function(n):
            yield _
            await asyncio.sleep(self.sleep_delay)
    
    def get_test_function(self, *decorations: Callable[[F], F]) -> F:
        func = self._test_subject
        for decorator in decorations:
            func = decorator(func)
        
        # noinspection PyTypeChecker
        return partial(func, self)
    # endregion
    # region Extended Assertions
    def assertEqual(self, actual: Any, expected: Any, msg: str = None):
        msg = f"{msg}: Expected: {expected!r}; Got: {actual!r}."
        return super().assertEqual(actual, expected, msg)

    def _assertIteration \
    (
        self,
        it: Iterator[int],
        iteration_name: str,
        *,
        assert_counting: bool = True,
        assert_stop_iteration: bool = True,
        expect_calls_increment: bool = True,
        expect_steps_increment: bool = True,
        steps: int = 5,
    ):
        counter = 0
        
        starting_calls_count = self.iterator_calls
        starting_steps_count = self.iterator_steps
        
        if (assert_counting):
            for _ in range(steps):
                with self.subTest(f"{iteration_name} iteration", step=counter):
                    item = next(it)
                    
                    self.assertEqual(item, counter, "Function result is broken")
                    self.assertEqual(self.iterator_calls, starting_calls_count + 1 * expect_calls_increment, "Generator calls mismatch")
                    self.assertEqual(self.iterator_steps, starting_steps_count + (counter + 1) * expect_steps_increment, "Generator iterators mismatch")
                
                counter += 1
        
        if (assert_stop_iteration):
            self.assertStopIteration(it, iteration_name)
    
    def assertStopIteration(self, it: Iterator[int], iteration_name: str, *, iter_name: str = None):
        prefix = "Iterator"
        if (iter_name is not None):
            prefix += f" {iter_name}"
        
        with self.subTest(f"{iteration_name} iteration", check='raises StopIteration'):
            with self.assertRaises(StopIteration, msg=f"{prefix} not stopped in time"):
                next(it)
    
    def assertRaceIteration(self, it: Iterator[int], start: int, end: int, iteration_name: str, *, expect_total_calls: int, expect_total_steps: int, iter_name: str = 'iter'):
        with self.subTest(f"{iteration_name} iteration"):
            for i in range(start, end):
                self.assertEqual(next(it), i, f"{iter_name} result mismatch")
            
            self.assertEqual(self.iterator_calls, expect_total_calls, f"{iteration_name} iteration calls count mismatch")
            self.assertEqual(self.iterator_steps, expect_total_steps, f"{iteration_name} iteration steps count mismatch")
    # endregion


__all__ = \
[
    'AbstractCachedIteratorTestCase',
]
