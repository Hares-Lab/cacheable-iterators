from typing import *
try:
    from unittest import IsolatedAsyncioTestCase
except ImportError:
    from aiounittest import AsyncTestCase as IsolatedAsyncioTestCase

from cacheable_iter.core import lru_async_iter_cache
from cacheable_iter.helpers import simple_cache
from .abstract_testcase import AbstractCachedIteratorTestCase

IntAsyncIteratorFunction = Callable[[int], AsyncIterator[int]]
class CachedAsyncIteratorTest(AbstractCachedIteratorTestCase, IsolatedAsyncioTestCase):
    @property
    def _test_subject(self) -> IntAsyncIteratorFunction:
        return type(self).async_iterator_function
    
    # region Extended Assertions
    async def assertIteration \
    (
        self,
        func: IntAsyncIteratorFunction,
        iteration_name: str,
        *,
        assert_counting: bool = True,
        assert_stop_iteration: bool = True,
        expect_calls_increment: bool = True,
        expect_steps_increment: bool = True,
        steps: int = 5,
    ):
        counter = 0
        it = func(steps)
        
        starting_calls_count = self.iterator_calls
        starting_steps_count = self.iterator_steps
        
        if (assert_counting):
            async for item in it:
                with self.subTest(f"{iteration_name} iteration", step=counter):
                    self.assertEqual(item, counter, "Function result is broken")
                    self.assertEqual(self.iterator_calls, starting_calls_count + 1 * expect_calls_increment, "Generator calls mismatch")
                    self.assertEqual(self.iterator_steps, starting_steps_count + (counter + 1) * expect_steps_increment, "Generator iterators mismatch")
                
                counter += 1
            else:
                with self.subTest(f"{iteration_name} iteration", check="Check steps count"):
                    self.assertEqual(counter, steps, "Wrong # of steps taken")
        
        if (assert_stop_iteration):
            await self.assertStopAsyncIteration(it, iteration_name)
    
    async def assertStopAsyncIteration(self, it: AsyncIterator[int], iteration_name: str, *, iter_name: str = None):
        prefix = "AsyncIterator"
        if (iter_name is not None):
            prefix += f" {iter_name}"
        
        with self.subTest(f"{iteration_name} iteration", check='raises StopAsyncIteration'):
            with self.assertRaises(StopAsyncIteration, msg=f"{prefix} not stopped in time"):
                await it.__anext__()
    
    async def assertAsyncRaceIteration(self, it: AsyncIterator[int], start: int, end: int, iteration_name: str, *, expect_total_calls: int, expect_total_steps: int, iter_name: str = 'iter'):
        with self.subTest(f"{iteration_name} iteration"):
            i = start
            if (i < end):
                async for item in it:
                    self.assertEqual(item, i, f"{iter_name} result mismatch")
                    i += 1
                    if (i >= end):
                        break
            
            self.assertEqual(i - start, end - start, "Wrong # of steps taken")
            
            self.assertEqual(self.iterator_calls, expect_total_calls, f"{iteration_name} iteration calls count mismatch")
            self.assertEqual(self.iterator_steps, expect_total_steps, f"{iteration_name} iteration steps count mismatch")
    # endregion
    # region Tests
    async def test_no_decorations(self):
        func = self.get_test_function()
        
        await self.assertIteration(func, "First")
        await self.assertIteration(func, "Second")
    
    async def test_simple_cache(self):
        func = self.get_test_function(simple_cache)
        
        await self.assertIteration(func, "First", assert_counting=True)
        await self.assertIteration(func, "Second", assert_counting=False)
    
    async def test_lru_async_iter_cache(self):
        func = self.get_test_function(lru_async_iter_cache)
        
        await self.assertIteration(func, "First", expect_steps_increment=True, expect_calls_increment=True)
        await self.assertIteration(func, "Second", expect_steps_increment=False, expect_calls_increment=False)
    
    async def test_lru_async_iter_cache_with_maxsize(self):
        func = self.get_test_function(lru_async_iter_cache(maxsize=2))
        
        await self.assertIteration(func, "First",  expect_steps_increment=True,  expect_calls_increment=True,  steps=3)
        await self.assertIteration(func, "Second", expect_steps_increment=True,  expect_calls_increment=True,  steps=4)
        await self.assertIteration(func, "Third",  expect_steps_increment=False, expect_calls_increment=False, steps=3)
        await self.assertIteration(func, "Fourth", expect_steps_increment=True,  expect_calls_increment=True,  steps=5)
        await self.assertIteration(func, "Fifth",  expect_steps_increment=False, expect_calls_increment=False, steps=3)
        await self.assertIteration(func, "Sixth",  expect_steps_increment=True,  expect_calls_increment=True,  steps=4)
    
    async def test_lru_async_iter_cache_race(self):
        func = self.get_test_function(lru_async_iter_cache)
        
        # Start
        iter1 = func(5)
        iter2 = func(5)
        
        await self.assertAsyncRaceIteration(iter1, 0, 0, "Initial", expect_total_calls=0, expect_total_steps=0)
        await self.assertAsyncRaceIteration(iter1, 0, 2, "First",   expect_total_calls=1, expect_total_steps=2, iter_name='iter1')
        await self.assertAsyncRaceIteration(iter2, 0, 2, "Second",  expect_total_calls=1, expect_total_steps=2, iter_name='iter2')
        await self.assertAsyncRaceIteration(iter2, 2, 4, "Third",   expect_total_calls=1, expect_total_steps=4, iter_name='iter2')
        await self.assertAsyncRaceIteration(iter1, 2, 5, "Fourth",  expect_total_calls=1, expect_total_steps=5, iter_name='iter1')
        await self.assertAsyncRaceIteration(iter2, 4, 5, "Fifth",   expect_total_calls=1, expect_total_steps=5, iter_name='iter2')
        await self.assertStopAsyncIteration(iter2, "Final", iter_name='iter2')
    # endregion


__all__ = \
[
    'CachedAsyncIteratorTest',
]


if (__name__ == '__main__'):
    from unittest import main
    main()
