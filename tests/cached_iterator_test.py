from typing import *

from cacheable_iter.core import iter_cache, lru_iter_cache
from cacheable_iter.helpers import simple_cache
from .abstract_testcase import AbstractCachedIteratorTestCase

IntIteratorFunction = Callable[[int], Iterator[int]]
class CachedIteratorTest(AbstractCachedIteratorTestCase[Iterator[int]]):
    @property
    def _test_subject(self) -> IntIteratorFunction:
        return type(self).iterator_function
    
    # region Extended Assertions
    def assertIteration(self, func: IntIteratorFunction, *args, steps: int = 5, **kwargs):
        return self._assertIteration(func(steps), *args, **kwargs, steps=steps)
    # endregion
    # region Tests
    def test_no_decorations(self):
        func = self.get_test_function()
        
        self.assertIteration(func, "First")
        self.assertIteration(func, "Second")
    
    def test_simple_cache(self):
        func = self.get_test_function(simple_cache)
        
        self.assertIteration(func, "First", assert_counting=True)
        self.assertIteration(func, "Second", assert_counting=False)
    
    def test_iter_cache(self):
        func = self.get_test_function(iter_cache)
        
        self.assertIteration(func, "First", expect_steps_increment=True, expect_calls_increment=True)
        self.assertIteration(func, "Second", expect_steps_increment=False, expect_calls_increment=False)
    
    def test_lru_iter_cache(self):
        func = self.get_test_function(lru_iter_cache(maxsize=2))
        
        self.assertIteration(func, "First",  expect_steps_increment=True,  expect_calls_increment=True,  steps=3)
        self.assertIteration(func, "Second", expect_steps_increment=True,  expect_calls_increment=True,  steps=4)
        self.assertIteration(func, "Third",  expect_steps_increment=False, expect_calls_increment=False, steps=3)
        self.assertIteration(func, "Fourth", expect_steps_increment=True,  expect_calls_increment=True,  steps=5)
        self.assertIteration(func, "Fifth",  expect_steps_increment=False, expect_calls_increment=False, steps=3)
        self.assertIteration(func, "Sixth",  expect_steps_increment=True,  expect_calls_increment=True,  steps=4)
    
    def test_iter_cache_race(self):
        func = self.get_test_function(iter_cache)
        
        # Start
        iter1 = func(5)
        iter2 = func(5)
        
        self.assertRaceIteration(iter1, 0, 0, "Initial", expect_total_calls=0, expect_total_steps=0)
        self.assertRaceIteration(iter1, 0, 2, "First",   expect_total_calls=1, expect_total_steps=2, iter_name='iter1')
        self.assertRaceIteration(iter2, 0, 2, "Second",  expect_total_calls=1, expect_total_steps=2, iter_name='iter2')
        self.assertRaceIteration(iter2, 2, 4, "Third",   expect_total_calls=1, expect_total_steps=4, iter_name='iter2')
        self.assertRaceIteration(iter1, 2, 5, "Fourth",  expect_total_calls=1, expect_total_steps=5, iter_name='iter1')
        self.assertRaceIteration(iter2, 4, 5, "Fifth",   expect_total_calls=1, expect_total_steps=5, iter_name='iter2')
        self.assertStopIteration(iter2, "Final", iter_name='iter2')
    # endregion


__all__ = \
[
    'CachedIteratorTest',
]


if (__name__ == '__main__'):
    from unittest import main
    main()
